package hust.soict.globalict.aims.order;
import hust.soict.globalict.aims.disc.DigitalVideoDisc;
import hust.soict.globalict.aims.utils.MyDate;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITED_ORDERS = 5;
	
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	
	private int qtyOrdered = 0;
	private MyDate dateOrdered;
	private static int nbOrders = 0;
	
	public Order() {
		if(nbOrders < MAX_LIMITED_ORDERS) {
			qtyOrdered = 0;
			dateOrdered = new MyDate();
			nbOrders++;
		}
		else {
			System.out.print("Exceed the number of orders!");
			nbOrders = nbOrders +1;
		}
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(nbOrders <= MAX_LIMITED_ORDERS) {
			if(qtyOrdered < MAX_NUMBERS_ORDERED) {
				itemsOrdered[qtyOrdered] = disc;
				qtyOrdered = qtyOrdered + 1;
				System.out.print("This disc have been added!\n");
			}
			else System.out.print("The order is almost full!\n");
		}
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		if(qtyOrdered == 0) {
			System.out.print("There is no disc!\n");
			return;
		}
		else {
			for(int i= 0; i<qtyOrdered; i++) {
				if(itemsOrdered[i] == disc) {
					for(int j=i; j<qtyOrdered -1; j++) {
						itemsOrdered[j] = itemsOrdered[j+1];
					}
					qtyOrdered--;
					System.out.println("Remove disc successfully," + " There is "+ qtyOrdered + "discs in the list!");
					return;
				}
			}
		}
	}
	
	public float totalCost() {
		float cost = 0;
		for(int i =0 ; i<qtyOrdered; i++) {
			cost += itemsOrdered[i].getCost();
		}
		return cost;
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList) {
		if(nbOrders <= MAX_LIMITED_ORDERS) {
			if(dvdList.length + qtyOrdered >= MAX_NUMBERS_ORDERED) {
				System.out.print("The list of disc cannot be added because of full ordered items!\n");
			}
			else {
				for(int i=0; i<dvdList.length; i++) {
					itemsOrdered[qtyOrdered] = dvdList[i];
					qtyOrdered = qtyOrdered +1;
				}
			}
		}
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		if(nbOrders <= MAX_LIMITED_ORDERS) {
			itemsOrdered[qtyOrdered] = dvd1;
			qtyOrdered = qtyOrdered +1;
			System.out.print("The first dvd has been added!\n");
		}
		else System.out.print("The order is almost full!\n");
		if(qtyOrdered < MAX_NUMBERS_ORDERED) {
			itemsOrdered[qtyOrdered] = dvd2;
			qtyOrdered = qtyOrdered +1;
			System.out.print("The second dvd has been added");
		}
		else System.out.print("The order is almost full!\n");
	}
	
	public void printing() {
		System.out.print("***************************************** ORDER *****************************************");
        System.out.println("Date: " + dateOrdered.getDay() + "/" + dateOrdered.getMonth() + "/" + dateOrdered.getYear() + "\nOrdered Items:");
        System.out.println("  Name - Title - Category - Director - Length : Price($)");		
        for(int i=0; i< qtyOrdered; i++) {
            System.out.println((i+1) + ".DVD - " + itemsOrdered[i].getTitle() + " - " + itemsOrdered[i].getCategory() + " - " + itemsOrdered[i].getDirector()+ " - " + itemsOrdered[i].getLength()+ " : " + itemsOrdered[i].getCost() + "$");
        }
        System.out.println("Total cost: " + totalCost());
        System.out.println("*****************************************************************************************");
	}
	
	public DigitalVideoDisc getALuckyItem() {
		int max = qtyOrdered-1;
		int min = 0;
		int range = max - min + 1;
		int rand = (int) (Math.random() * range) + min;
		System.out.println("Index of lucky item:" + rand);
		return itemsOrdered[rand];
	}
	
}
