package hust.soict.globalict.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable{
    
	private String artist;
	private ArrayList<Track> tracks = new ArrayList<Track>();
	
	public CompactDisc(String title, String category, float cost, int length, String director, String artist){
		super(title, category, cost, length, director);
		this.artist = artist;
	}
	
	public CompactDisc(String title, String category, float cost, int length, String director, String artist, ArrayList<Track> tracks) {
		super(title, category, cost, length, director);
		this.artist = artist;
		this.tracks = tracks;
	}
	
	public String getArtist() {
		return this.artist;
	}
	
	public void addTrack(Track newTrack) {
		for(Track track: this.tracks) {
			if(newTrack.getTitle().equals(track.getTitle())) {
				System.out.println("The track [" + newTrack.getTitle() +"]  is already in the list");
				System.out.println();
				return ;
			}
			
			this.tracks.add(newTrack);
			this.setLength(this.getLength() + newTrack.getLength());
			System.out.println("The track [" + newTrack.getTitle() + "] has been added!");
		}
	}
	
	public void removeTrack(Track removeTrack) {
		int checkflag = 0;
		for(Track track : tracks) {
			if(removeTrack.getTitle().equals(track.getTitle())) {
				tracks.remove(removeTrack);
				checkflag =1;
				break;
			}
			
			if(checkflag == 0) {
				System.out.println("The track [" + removeTrack.getTitle() +"] doesn't exist in the list!");
			}
			else {
				System.out.println("The track [ " + removeTrack.getTitle() + "] has been removed from the list");
			}
		}
	}
	
	public int getLength() {
		int sum = 0;
		for(Track track: tracks) {
			sum += track.getLength();		
		}
		return sum;
	}
	
	public void play() {
		System.out.println("*** The title of the currently playing compact disc: [" + this.getTitle() + "] ***");
		System.out.println(" ===>>> Track list <<<===");
		System.out.println();
		for(Track track: tracks) {
			track.play();
		}
	}
}