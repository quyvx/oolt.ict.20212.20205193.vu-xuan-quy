package package1;

public class TestPassingParameter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle");
		DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella");
//		changeTitle(jungleDVD, "Cinderella");
		swap(jungleDVD, cinderellaDVD);
		System.out.println("Jungle dvd title: "+ jungleDVD.getTitle());
		System.out.println("Cinderella dvd title: "+ cinderellaDVD.getTitle());
	}
	
//	public static void swap(Object o1, Object o2) {
//		Object tem = o1;
//		o1 = o2;
//		o2 = tem;
//	}
	
	public static void swap(DigitalVideoDisc o1, DigitalVideoDisc o2) {
		DigitalVideoDisc tem = new DigitalVideoDisc(o1.getTitle());
		//swap title
		
		o1.setTitle(o2.getTitle());
		o2.setTitle(tem.getTitle());
		//swap category
		tem.setCategory(o1.getCategory());
		o1.setCategory(o2.getCategory());
		o2.setCategory(tem.getCategory());
		//swap director
		tem.setDirector(o1.getDirector());
		o1.setDirector(o2.getDirector());
		o2.setDirector(tem.getDirector());
		//swap length
		tem.setLength(o1.getLength());
		o1.setLength(o2.getLength());
		o2.setLength(tem.getLength());
		//swap cost
		tem.setCost(o1.getCost());
		o1.setCost(o2.getCost());
		o2.setCost(tem.getCost());
	}
	
	public static void changeTitle(DigitalVideoDisc dvd, String title) {
		String oldTitle = dvd.getTitle();
		dvd.setTitle(title);
		dvd = new DigitalVideoDisc(oldTitle);
	}
}
