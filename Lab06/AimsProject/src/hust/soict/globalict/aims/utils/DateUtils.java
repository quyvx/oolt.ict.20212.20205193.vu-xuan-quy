package hust.soict.globalict.aims.utils;

import java.util.Arrays;

public class DateUtils {
    public static int compareTwoDates(MyDate date1, MyDate date2) {
        if (date1.getYear() < date2.getYear()) {
            return -1;
        } else if (date1.getYear() > date2.getYear()) {
            return 1;
        } else {
            if (date1.getMonth() < date2.getMonth()) {
                return -1;
            } else if (date1.getMonth() > date2.getMonth()) {
                return 1;
            } else {
                return Integer.compare(date1.getDay(), date2.getDay());
            }
        }
    }

    public static void sortDates(MyDate[] dates) {
        Arrays.sort(dates, DateUtils::compareTwoDates);
    }
}
