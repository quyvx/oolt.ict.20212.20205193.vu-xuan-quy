package package1;

import java.util.Arrays;

public class DateUtils {
	public static int compare(MyDate date1, MyDate date2) {
		if(date1.getYear() < date2.getYear())
			return -1;
		if(date1.getYear() > date2.getYear())
			return 1;
		if(date1.getMonth() < date2.getMonth())
			return -1;
		if(date1.getMonth() > date2.getMonth())
			return 1;
		return Integer.compare(date1.getDay(), date2.getDay());
	}
	
	public static void dateCompare(MyDate date1,MyDate date2) {
		if (date1.year<date2.year) {
			System.out.println(date1.printDate()+" begins earlier than "+ date2.printDate());
		}else if (date1.year>date2.year)
			System.out.println(date1.printDate()+" begins later than "+ date2.printDate());
		else {
			if (date1.month<date2.month) {
				System.out.println(date1.printDate()+" begins earlier than "+ date2.printDate());
			}else if (date1.month>date2.month)
				System.out.println(date1.printDate()+" begins later than "+ date2.printDate());
			else {
				if (date1.day<date2.day) {
					System.out.println(date1.printDate()+" begins earlier than "+ date2.printDate());
				}else if (date1.day>date2.day)
					System.out.println(date1.printDate()+" begins later than "+ date2.printDate());
				else {
					System.out.println("These two days are the same");
				}
			}
		}
	}
	
	public static void sort(MyDate[] date) {
		Arrays.sort(date, DateUtils::compare);
	}
}
