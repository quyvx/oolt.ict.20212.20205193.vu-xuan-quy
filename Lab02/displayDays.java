import javax.swing.JOptionPane;

public class displayDays{
    public static void main(String[] args) {
        String yearString;
        int year;
        boolean isLeapYear;
        String monthString;
        int days = 100;
        boolean isValidMonth = true;
        boolean invalidPreviousInput = false;

        yearString = JOptionPane.showInputDialog(null, "Please enter year: ", "Year", JOptionPane.INFORMATION_MESSAGE);
        year = Integer.parseInt(yearString);
        while(year < 0) {
            yearString = JOptionPane.showInputDialog(null, "Invalid year! Please enter again: ", "Year", JOptionPane.INFORMATION_MESSAGE);
            year = Integer.parseInt(yearString);
        }
        
        if(year % 4 == 0) {
            if(year % 100 == 0 && year % 400 != 0) {
                isLeapYear = false;
            }else {
                isLeapYear = true;
            }
        }else {
            isLeapYear = false;
        }
        
        do {
            isValidMonth = true;
            String message;
            if(!invalidPreviousInput) {
                message = "Please enter month: ";
            }else {
                message = "Invalid month! Please enter again: ";
            }
            monthString = JOptionPane.showInputDialog(null, message, "Month", JOptionPane.INFORMATION_MESSAGE);
            switch(monthString) {
                case "1":
                case "Jan":
                case "Jan.":
                case "January":
                days = 31;
                break;
                case "2":
                case "Feb":
                case "Feb.":
                case "February":
                if(isLeapYear) {
                    days = 29;
                }else {
                    days = 28;
                }
                break;
                case "3":
                case "Mar":
                case "Mar.":
                case "March":
                days = 31;
                break;
                case "4":
                case "Apr":
                case "Apr.":
                case "April":
                days = 30;
                break;
                case "5":
                case "May":
                days = 31;
                break;
                case "6":
                case "June":
                case "Jun":
                days = 30;
                break;
                case "7":
                case "July":
                case "Jul":
                days = 31;
                break;
                case "8":
                case "Aug":
                case "Aug.":
                case "August":
                days = 31;
                break;
                case "9":
                case "Sep":
                case "Sept.":
                case "September":
                days = 30;
                break;
                case "10":
                case "Oct":
                case "Oct.":
                case "October":
                days = 31;
                break;
                case "11":
                case "Nov":
                case "Nov.":
                case "November":
                days = 30;
                break;
                case "12":
                case "Dec":
                case "Dec.":
                case "December":
                days = 31;
                break;
                default: 
                isValidMonth = false;
                invalidPreviousInput = true;
            }
        }while(!isValidMonth);

        JOptionPane.showMessageDialog(null, "The number of days is: " + Integer.toString(days), "Result",JOptionPane.INFORMATION_MESSAGE);
    }
}
