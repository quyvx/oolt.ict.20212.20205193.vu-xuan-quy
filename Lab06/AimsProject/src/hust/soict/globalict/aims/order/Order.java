package hust.soict.globalict.aims.order;
import java.util.ArrayList;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITED_ORDERS = 5;
	
	//private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	
	
	private int qtyOrdered = 0;
	private MyDate dateOrdered;
	private static int nbOrders = 0;
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	
	public static int getNumberOfOrder() {
		return nbOrders;
	}
	
	public int getQtyOrdered() {
		return this.qtyOrdered;
	}
	
	public Media getMedia(int index) {
		return itemsOrdered.get(index);
	}
	
	
	public Order() {
		if(nbOrders < MAX_LIMITED_ORDERS) {
			qtyOrdered = 0;
			dateOrdered = new MyDate();
			nbOrders++;
		}
		else {
			System.out.print("Exceed the number of orders!");
			nbOrders = nbOrders +1;
		}
	}
	
	/*public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(nbOrders <= MAX_LIMITED_ORDERS) {
			if(qtyOrdered < MAX_NUMBERS_ORDERED) {
				itemsOrdered[qtyOrdered] = disc;
				qtyOrdered = qtyOrdered + 1;
				System.out.print("This disc have been added!\n");
			}
			else System.out.print("The order is almost full!\n");
		}
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		if(qtyOrdered == 0) {
			System.out.print("There is no disc!\n");
			return;
		}
		else {
			for(int i= 0; i<qtyOrdered; i++) {
				if(itemsOrdered[i] == disc) {
					for(int j=i; j<qtyOrdered -1; j++) {
						itemsOrdered[j] = itemsOrdered[j+1];
					}
					qtyOrdered--;
					System.out.println("Remove disc successfully," + " There is "+ qtyOrdered + "discs in the list!");
					return;
				}
			}
		}
	}*/
	
	public float totalCost() {
		float cost = 0;
		for(Media media: itemsOrdered) {
			cost += media.getCost();
		}
		return cost;
	}
	
	/*public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList) {
		if(nbOrders <= MAX_LIMITED_ORDERS) {
			if(dvdList.length + qtyOrdered >= MAX_NUMBERS_ORDERED) {
				System.out.print("The list of disc cannot be added because of full ordered items!\n");
			}
			else {
				for(int i=0; i<dvdList.length; i++) {
					itemsOrdered[qtyOrdered] = dvdList[i];
					qtyOrdered = qtyOrdered +1;
				}
			}
		}
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		if(nbOrders <= MAX_LIMITED_ORDERS) {
			itemsOrdered[qtyOrdered] = dvd1;
			qtyOrdered = qtyOrdered +1;
			System.out.print("The first dvd has been added!\n");
		}
		else System.out.print("The order is almost full!\n");
		if(qtyOrdered < MAX_NUMBERS_ORDERED) {
			itemsOrdered[qtyOrdered] = dvd2;
			qtyOrdered = qtyOrdered +1;
			System.out.print("The second dvd has been added");
		}
		else System.out.print("The order is almost full!\n");
	}*/
	
	public void addMedia(Media media) {
		if(qtyOrdered < MAX_NUMBERS_ORDERED) {
			itemsOrdered.add(media);
			qtyOrdered++;
			System.out.println("The new media has been added successfully");
		}
		else {
			System.out.println("This order is already full");
		}
			
	}
	
	public void removeMedia(Media media) {
		if(qtyOrdered == 0) {
			System.out.println("Empty media list!");
		}
		else {
			itemsOrdered.remove(media);
			qtyOrdered --;
			System.out.println("Removed [" + media.getTitle() + "] successfully");
		}
	}
	
	public void removeMedia(int index) {
		if(qtyOrdered == 0) {
			System.out.println("Empty media list!");
		}
		else {
			itemsOrdered.remove(itemsOrdered.get(index));
			qtyOrdered--;
			System.out.println("Removed [" + index +"] successfully");
		}
	}
	
	public void removeMedia(String id) {
        if (qtyOrdered == 0) {
            System.out.println("Empty media list!");
        } else {
            int checkflag = 0;
            for (Media item: itemsOrdered) {
                String comID = new String(item.getId());
                if(id.equals(comID)) {
                    itemsOrdered.remove(item);
                    qtyOrdered--;
                    checkflag = 1;
                    break;
                }
            }
            
            if (checkflag == 1) System.out.println("Removed [" + id + "] successfully!");
            else System.out.println("Cannot find the media with the ID [" + id + "] in this order list!" );
        }		
	}

    public void printOrder() {
        System.out.println("*************************Order*************************");
        System.out.println("Date: " + this.dateOrdered.getDay() + " - " + this.dateOrdered.getMonth() + " - " + this.dateOrdered.getYear());
        for (int i = 0; i < itemsOrdered.size(); i++) {
            Media item = itemsOrdered.get(i);
            System.out.println((i + 1) + ". " + item.getId() + " - " + item.getTitle() + " - " + item.getCategory() + " - " + item.getCost() + "$");
        }
        System.out.println("Total cost: " + this.totalCost());
        System.out.println("*******************************************************");
    }
	
    public Media getALuckyItem() {
        int luckyItemIndex = (int)(Math.random() * itemsOrdered.size());
        Media luckyItem = itemsOrdered.get(luckyItemIndex);
        removeMedia(luckyItem.getId());
        return luckyItem;
    }
}
