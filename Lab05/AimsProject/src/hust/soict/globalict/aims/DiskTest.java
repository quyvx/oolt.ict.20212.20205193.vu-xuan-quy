package hust.soict.globalict.aims;

import hust.soict.globalict.aims.disc.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;

public class DiskTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Order anOrder = new Order();
		//Create a new DVD object and set the fields
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		//add the dvd1 to the order
		//anOrder.addDigitalVideoDisc(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		
		anOrder.addDigitalVideoDisc(dvd1, dvd2);
		anOrder.addDigitalVideoDisc(dvd2, dvd1);
		
		//test search method
		System.out.println(dvd1.search("Lion The"));
		System.out.println(dvd2.search("Star"));
		System.out.println(dvd2.search("Lion"));
		
		
		//test random lucky item
		//In order
		// --> print total cost
		System.out.print(anOrder.totalCost());
		//System.out.print(anOrder.getALuckyItem().getTittle());
	}

}