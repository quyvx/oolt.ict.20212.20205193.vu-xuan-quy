package package1;

public class Order{
	public static final int MAX_NUMBERS_ORDERED = 10;
	private DigitalVideoDisc itemOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	int qtyOrdered = 0;
	public static final int MAX_LIMITTED_ORDERS = 5;
	private static int nbOrders = 0;
	private MyDate dateOrdered;
	public Order() {
		MyDate date = new MyDate();
		this.dateOrdered = date;
		if(nbOrders<MAX_LIMITTED_ORDERS)
			nbOrders++;
		else 
			System.out.println("You can not add any order due to limitation reacched");
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(qtyOrdered == MAX_NUMBERS_ORDERED) {
			System.out.println("The order is full"
					+ "There is "+ qtyOrdered +" discs in the list!");
			return;	
		}else {
			itemOrdered[qtyOrdered] = disc;
			qtyOrdered++;
			System.out.println("The disc has been added."
					+ "There is "+ qtyOrdered +" discs in the list!");
		}
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		if(qtyOrdered == 0) {
			System.out.println("There is no disc!");
			return;
		}else {
			for(int i = 0; i<qtyOrdered; i++) {
				if(itemOrdered[i] == disc) {
					for(int j = i; j<qtyOrdered-1; j++) {
						itemOrdered[j] = itemOrdered[j+1];
					}
					qtyOrdered--;
					System.out.println("Remove disc successfully."
							+ " There is "+ qtyOrdered +" discs in the list!");
					return;
				}
			}
		}
	}
	
	public float totalCost() {
		float cost = 0;
		for (int i = 0; i<qtyOrdered; i++) {
			cost += itemOrdered[i].getCost();
		}
		return cost;
	}

//	public void print() {
//		System.out.printf("\n%-20s%-20s%-10s%-20s%s\n","Title","Category","Cost","Director","Length");
//		for(int i=0; i<qtyOrdered;i++) {
//			System.out.printf("%-20s%-20s%-10.2f%-20s%d\n",
//					itemOrdered[i].getTitle(), itemOrdered[i].getCategory(),
//					itemOrdered[i].getCost(), itemOrdered[i].getDirector(),
//					itemOrdered[i].getLength());
//		}
//	}
	
	public void printOrder() {
		System.out.println("*******************************Order***********************************");
		dateOrdered.print();
		System.out.println("Ordered Items: ");
		for(int i = 0; i<qtyOrdered; i++) {
			System.out.println((i+1) + ". DVD - ["+itemOrdered[i].getTitle()+ "] - ["+
					itemOrdered[i].getCategory() + "] - [" +
					itemOrdered[i].getDirector() + "] - [" +
					itemOrdered[i].getLength() + "]: [" +
					itemOrdered[i].getCost() + "] $");
		}
		System.out.println("Total Cost: " + totalCost());
		System.out.println("**********************************************************************");
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
		if(qtyOrdered == MAX_NUMBERS_ORDERED) {
			System.out.println("The order is full. "
					+ "There is "+ qtyOrdered +" discs in the list!");
			return;	
		}else {
			if(dvdList.length <= MAX_NUMBERS_ORDERED - qtyOrdered) {
				for(int i = 0;i<dvdList.length;i++) {
					itemOrdered[qtyOrdered] = dvdList[i];
					qtyOrdered ++;
				}
				System.out.println(dvdList.length +" has been added to the order!"
						+ " There is "+ qtyOrdered +" discs in the list!");
			}
		}
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc1, DigitalVideoDisc disc2) {
		if(qtyOrdered == MAX_NUMBERS_ORDERED) {
			System.out.println("The order is full"
					+ "There is "+ qtyOrdered +" discs in the list!");
			return;	
		}else {
			if(MAX_NUMBERS_ORDERED - qtyOrdered >=2) {
				itemOrdered[qtyOrdered] = disc1;
				qtyOrdered++;
				itemOrdered[qtyOrdered] = disc2;
				qtyOrdered++;
				System.out.println("2 disc have been added."
						+ "There is "+ qtyOrdered +" discs in the list!");
			}
		}
	}
}