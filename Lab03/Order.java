package package1;

public class Order{
	public static final int MAX_NUMBERS_ORDERED =10;
	
	private DigitalVideoDisc itemsOrdered[]=new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	
	private int qtyOrdered =0;
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		itemsOrdered[qtyOrdered++]=disc;
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		DigitalVideoDisc[] newItemsOrdered =new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
		for(int i=0, k=0;i<qtyOrdered; i++) {
			if(itemsOrdered[i].getTitle().equals(disc.getTitle())) {
				continue;
			}
			newItemsOrdered[k++]=itemsOrdered[i];
			
		}
		itemsOrdered =newItemsOrdered;
	}
	
	public float totalCost() {
		float cost =0;
		for(int i=0; i<qtyOrdered; i++) {
			cost += itemsOrdered[i].getCost();
		}
		return cost;
	}
}