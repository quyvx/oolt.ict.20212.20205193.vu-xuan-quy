package hust.soict.globalict.aims.media;

public class DigitalVideoDisc extends Disc implements Playable{
	//private String title;
	//private String category;
	//private String director;
	//private int length;
	//private float cost;
	
	public DigitalVideoDisc(String title, String category, float cost, int length, String director){
		super(title, category, cost, length, director);
	}

	public DigitalVideoDisc(DigitalVideoDisc dvd){
		super(dvd.getTitle(), dvd.getCategory(), dvd.getCost(), dvd.getLength(), dvd.getDirector() );
	}

	public void play(){
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}

	//public DigitalVideoDisc(String title) {
		//super();
		//this.title = title;
	//}
	
	//public DigitalVideoDisc(String title, String category) {
		//super();
		//this.title = title;
		//this.category = category;
	//}
	
	//public DigitalVideoDisc(String title, String category, String director) {
		//super();
		//this.title = title;
		//this.category = category;
		//this.director = director;
	//}
	
	//public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		//super();
		//this.title = title;
		//this.category = category;
		//this.director = director;
		//this.length = length;
		//this.cost = cost;
	//}
	
	//public String getTitle() {
		//return title;
	//}
	
	//public void setTitle(String title) {
		//this.title = title;
	//}
	
	//public String getCategory() {
		//return category;
	//}
	
	//public void setCategory(String category) {
		//this.category = category;
	//}
	

	/*public DigitalVideoDisc() {
		super("","",0.0f, "");
		this.director = director;
		this.length = length;
	}
	
	public DigitalVideoDisc(String title) {
		super(title, "", 0.0f, "");
		
	}
	
	public DigitalVideoDisc(String title, String category) {
		super(title, category, 0.0f, "");
	}
	
	public DigitalVideoDisc(String title, String category, String director) {
		super(title, category, 0.0f, "");
		this.director = director;
	}
	
	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		super(title, category, cost, "");
		this.director = director;
		this.length = length;
	}
	
	public DigitalVideoDisc(String id,String title, String category, String director, int length, float cost) {
		super(title, category, cost, id);
		this.director = director;
		this.length = length;
	}
	
	public DigitalVideoDisc(DigitalVideoDisc dvd) {
		super(dvd.getTitle(), dvd.getCategory(), dvd.getCost(), dvd.getId());
		this.director = director;
		this.length = length;
	}
	
	public String getDirector() {
		return director;
	}
	
	public void setDirector(String director) {
		this.director = director;
	}
	
	public int getLength() {
		return length;
	}
	
	public void setLength(int length) {
		this.length = length;
	}
	
	//public float getCost() {
		//return cost;
	//}
	
	//public void setCost(float cost) {
		//this.cost = cost;
	//}
	
	public boolean search(String title) {
		//String convertedTitle = this.title.toLowerCase();
		String[] listSubtitle = title.split(" ");
		for (String s: listSubtitle) {
			s = s.toLowerCase();
			//if (!convertedTitle.contains(s))
			//return false;
		}	
		//try use equals/containsIgnoreCase
		return true;
		
	}
	*/

	public void printInfo() {
        System.out.println("----------DVD INFO----------");
        System.out.println("Title: " + this.getTitle());
        System.out.println("Category: " + this.getCategory());
        System.out.println("Director: "+ this.getDirector());
        System.out.println("Length: " + this.getLength());
        System.out.println("Cost: " + this.getCost());
    }
	
}
