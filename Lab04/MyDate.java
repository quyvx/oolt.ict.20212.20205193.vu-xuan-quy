package package1;

import java.util.Scanner;
public class MyDate {
	int day;
	int month;
	int year;
	
	public MyDate(int day, int month, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public MyDate(String date) {
		String[] currentDate = date.split(" ");
		String inputMonth = currentDate[0];
		String inputDay = currentDate[1];
		String inputYear = currentDate[2]; 

        if (inputMonth.matches("January|Jan.|Jan|1"))
        	this.month = 1;
        else if (inputMonth.matches("February|Feb.|Feb|2"))
        	this.month = 2;
        else if (inputMonth.matches("March|Mar.|Mar|3"))
        	this.month = 3;
        else if (inputMonth.matches("April|Apr.|Apr|4"))
        	this.month = 4;
        else if (inputMonth.matches("May|5"))
        	this.month = 5;
        else if (inputMonth.matches("June|Jun|6"))
        	this.month = 6;
        else if (inputMonth.matches("July|Jul|7"))
        	this.month = 7;
        else if (inputMonth.matches("August|Aug.|Aug|8"))
        	this.month = 8;
        else if (inputMonth.matches("September|Sep.|Sep|9"))
        	this.month = 9;
        else if (inputMonth.matches("October|Oct.|Oct|10"))
        	this.month = 10;
        else if (inputMonth.matches("November|Nov.|Nov|11"))
        	this.month = 11;
        else if (inputMonth.matches("December|Dec.|Dec|12"))
        	this.month = 12;
        else
        	this.month = 0;
        
		this.day = Integer.parseInt(inputDay);
		this.year = Integer.parseInt(inputYear);
		
	}
	
	public MyDate() {
		//System.out.println(java.time.LocalDate.now());
		this.year = Integer.parseInt(java.time.LocalDate.now().toString().substring(0,4));
		this.month = Integer.parseInt(java.time.LocalDate.now().toString().substring(5,7));
		this.day = Integer.parseInt(java.time.LocalDate.now().toString().substring(8,10));
		
//		String[] currentDate = java.time.LocalDate.now().toString().split("-");
//        this.day = Integer.parseInt(currentDate[2]);
//        this.month = Integer.parseInt(currentDate[1]);
//        this.year = Integer.parseInt(currentDate[0]);
	}

	public MyDate(String day, String month, String year) {
		
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	public void accept() {
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter a date (month day year): ");
		String date = sc.nextLine();
		
        String[] currentDate = date.split(" ");

        setDay(Integer.parseInt(currentDate[1]));
        setYear(Integer.parseInt(currentDate[2]));
        if (currentDate[0].matches("January|Jan.|Jan|1"))
        	setMonth(1);
        else if (currentDate[0].matches("February|Feb.|Feb|2"))
        	setMonth(2);
        else if (currentDate[0].matches("March|Mar.|Mar|3"))
        	setMonth(3);
        else if (currentDate[0].matches("April|Apr.|Apr|4"))
        	setMonth(4);
        else if (currentDate[0].matches("May|5"))
        	setMonth(5);
        else if (currentDate[0].matches("June|Jun|6"))
        	setMonth(6);
        else if (currentDate[0].matches("July|Jul|7"))
        	setMonth(7);
        else if (currentDate[0].matches("August|Aug.|Aug|8"))
        	setMonth(8);
        else if (currentDate[0].matches("September|Sep.|Sep|9"))
        	setMonth(9);
        else if (currentDate[0].matches("October|Oct.|Oct|10"))
        	setMonth(10);
        else if (currentDate[0].matches("November|Nov.|Nov|11"))
        	setMonth(11);
        else if (currentDate[0].matches("December|Dec.|Dec|12"))
        	setMonth(12);
        sc.close();
		
	}
	
	public void print() {
		System.out.println("Date: "+ day +"/"+ month+"/"+year);
	}
	public String printDate() {
		return day+"/"+month+"/"+year; 
	}
}
