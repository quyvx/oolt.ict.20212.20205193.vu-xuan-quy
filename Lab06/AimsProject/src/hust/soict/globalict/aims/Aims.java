package hust.soict.globalict.aims;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;

public class Aims {
	/*public static void main(String[] args) {
		Order anOrder = new Order();
		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		anOrder.addDigitalVideoDisc(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		anOrder.addDigitalVideoDisc(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		anOrder.addDigitalVideoDisc(dvd3);
		
		System.out.print("Total Cost is: ");
		System.out.println(anOrder.totalCost());
		
	}*/
	
	public static void main(String[] args) {
		ArrayList<Order> anOrder = new ArrayList<Order>();
		
		while(true) {
			Scanner sc = new Scanner(System.in);
			System.out.println("Order Management Application: ");
			System.out.println("--------------------------------");
			System.out.println("1.Create new order");
			System.out.println("2.Add item to the order");
			System.out.println("3.Delete item by id");
			System.out.println("4.Display the items list og order");
			System.out.println("0.Exit");
			System.out.println("--------------------------------");
			System.out.println("Please choose a number: 0-1-2-3-4: ");
			
			int choice = sc.nextInt();
			
			switch(choice) {
				case 1:
					System.out.println("\n1. Create new order\n");
					if(Order.getNumberOfOrder() >= 5) {
						System.out.println("Full of orders!\n");
						break;
					}
					Order newOrder = new Order();
					anOrder.add(newOrder);
					System.out.println("Number of existing orders: " + anOrder.size() + "\n");
					break;
				case 2:
					if(Order.getNumberOfOrder() == 0) {
						System.out.println("\nNo existing order! Please create a new order first!\n");
						break;
					}
					
					System.out.println("\n2.Add item to the order\n");
					System.out.println("Please enter 1. Disc 2.Book");
					System.out.println(" ---> ");
					int type = sc.nextInt();
					
					if( type == 1) {
						System.out.println("Please enter the information of the disc following this informat");
						System.out.println("---<ID - Title - Category - Director - Length - Cost>---");
						System.out.println("--> ID: \n");
						String nID = sc.next();
						sc.nextLine();
						System.out.println("--> Title: \n");
						String nTitle = sc.nextLine();
						//sc.nextLine();
						System.out.println("--> Category: \n");
						String nCategory = sc.nextLine();
						//sc.next();
						System.out.println("--> Director: \n");
						String nDirector = sc.nextLine();
						//sc.nextLine();
						System.out.println("--> Length");
						int nLength = sc.nextInt();
						sc.nextLine();
						System.out.println("--> Cost");
						float nCost = sc.nextFloat();
						
						DigitalVideoDisc newDisc = new DigitalVideoDisc(nID, nTitle, nCategory, nDirector, nLength, nCost);
						anOrder.get(anOrder.size()-1).addMedia(newDisc);
						}
					else if(type == 2) {
							System.out.println("Please enter the information of the book following this format");
							System.out.println("---<ID - Title - Category - Number of author - Authors' list - Cost>---");
							System.out.println("-->ID");
							String nID = sc.next();
							sc.nextLine();
							System.out.println("--> Title: ");
							String nTitle = sc.nextLine();
							//sc.nextLine();
							System.out.println("--> Category: ");
							String nCategory = sc.nextLine();
							//sc.nextLine();
							System.out.println("--> Enter the number of authors: ");
							int numberOfAuthors = sc.nextInt();
							sc.nextLine();
							System.out.println("--> Enter the authors' list");
							List<String> Authors = new ArrayList<String>();
							for(int i = 0; i< numberOfAuthors; i++) {
								String nAuthors = sc.nextLine();
								Authors.add(nAuthors);
							}
							
							System.out.println("--> Cost: ");
							float nCost = sc.nextFloat();
							
							Book  newBook = new Book(nID, nTitle, nCategory, nCost, Authors);
							anOrder.get(anOrder.size()-1).addMedia(newBook);
						}
					else {
						System.out.println("Wrong choice!");
					}
					break;
				case 3:
					if(Order.getNumberOfOrder() == 0) {
						System.out.println("\nNo exisiting order! Please create a new order first!\n");
						break;
					}
					
					int checkflag = 0;
					
					System.out.println("\n3.Delete item by id");
					System.out.println("--> Please enter the id of the media you want to delete: ");
					String ID = sc.next();
					sc.nextLine();
					
					for(int i =0 ; i < anOrder.get(anOrder.size() - 1).getQtyOrdered(); i++) {
						if(ID.equals(anOrder.get(anOrder.size()-1).getQtyOrdered())) {
							anOrder.get(anOrder.size()-1).removeMedia(anOrder.get(anOrder.size()-1).getMedia(i));
							checkflag =1;
							break;
						}
					}
					
					anOrder.get(anOrder.size()-1).removeMedia(ID);
					
					if(checkflag == 0) System.out.println("Cannot find the media in the list");
					break;
				case 4:
					if(Order.getNumberOfOrder() == 0) {
						System.out.println("\nNo exisiting order ! Please create a new order first!\n");
						break;
					}
					
					int numList = 0;
					
					do {
						System.out.println("\n4. Display the items list of order\n");
						System.out.println("--> Enter the order list's number: ");
						numList = sc.nextInt();
					}while(numList <  1 || numList >Order.getNumberOfOrder());
					anOrder.get(numList -1).printOrder();
					break;
				case 0: 
					System.out.println("\n0.Exit\n");
					sc.close();
					System.out.println("Exisiting the program !");
					System.exit(0);
				default: 
					System.out.println("\nWrong choice! Please make the right selection!\n");
					break;
			} 
		}
	}	
}
