import java.util.Scanner;

public class displayATriangle{
    public static void main(String args[]){
        Scanner n = new Scanner(System.in);
        System.out.println("Enter n:");
        int height =n.nextInt();

        for(int i =1; i<=5; i++){
            for(int j=0;j <= height-i; j++ ){
                System.out.print(" ");
            }
            for(int j=1; j<=2*i-1; j++){
                System.out.print("*");
            }
            for(int j=0;j <= height-i; j++ ){
                System.out.print(" ");
            }
            System.out.println();
        }

    }
}